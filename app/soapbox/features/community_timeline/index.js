import React from 'react';
import { connect } from 'react-redux';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import StatusListContainer from '../ui/containers/status_list_container';
import Column from '../../components/column';
import ColumnSettings from './containers/column_settings_container';
import { expandCommunityTimeline } from '../../actions/timelines';
import { connectCommunityStream } from '../../actions/streaming';
import { getSettings } from 'soapbox/actions/settings';
import SubNavigation from 'soapbox/components/sub_navigation';

const messages = defineMessages({
  title: { id: 'column.community', defaultMessage: 'Local timeline' },
});

const mapStateToProps = state => {
  const onlyMedia = getSettings(state).getIn(['community', 'other', 'onlyMedia']);
  const withMuted = getSettings(state).getIn(['community', 'other', 'withMuted']);

  const timelineId = 'community';

  return {
    timelineId,
    onlyMedia,
    withMuted,
    hasUnread: state.getIn(['timelines', `${timelineId}${onlyMedia ? ':media' : ''}${withMuted ? ':wmuted' : ''}`, 'unread']) > 0,
  };
};

export default @connect(mapStateToProps)
@injectIntl
class CommunityTimeline extends React.PureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    intl: PropTypes.object.isRequired,
    hasUnread: PropTypes.bool,
    onlyMedia: PropTypes.bool,
    withMuted: PropTypes.bool,
    timelineId: PropTypes.string,
  };

  componentDidMount() {
    const { dispatch, onlyMedia, withMuted } = this.props;
    dispatch(expandCommunityTimeline({ onlyMedia, withMuted }));
    this.disconnect = dispatch(connectCommunityStream({ onlyMedia, withMuted }));
  }

  componentDidUpdate(prevProps) {
    if (prevProps.onlyMedia !== this.props.onlyMedia || prevProps.withMuted !== this.props.withMuted) {
      const { dispatch, onlyMedia, withMuted } = this.props;

      this.disconnect();
      dispatch(expandCommunityTimeline({ onlyMedia, withMuted }));
      this.disconnect = dispatch(connectCommunityStream({ onlyMedia, withMuted }));
    }
  }

  componentWillUnmount() {
    if (this.disconnect) {
      this.disconnect();
      this.disconnect = null;
    }
  }

  handleLoadMore = maxId => {
    const { dispatch, onlyMedia, withMuted } = this.props;
    dispatch(expandCommunityTimeline({ maxId, onlyMedia, withMuted }));
  }

  handleRefresh = () => {
    const { dispatch, onlyMedia } = this.props;
    return dispatch(expandCommunityTimeline({ onlyMedia }));
  }


  render() {
    const { intl, onlyMedia, withMuted, timelineId } = this.props;

    return (
      <Column label={intl.formatMessage(messages.title)} transparent>
        <SubNavigation message={intl.formatMessage(messages.title)} settings={ColumnSettings} />
        <StatusListContainer
          scrollKey={`${timelineId}_timeline`}
          timelineId={`${timelineId}${onlyMedia ? ':media' : ''}${withMuted ? ':wmuted' : ''}`}
          onLoadMore={this.handleLoadMore}
          onRefresh={this.handleRefresh}
          emptyMessage={<FormattedMessage id='empty_column.community' defaultMessage='The local timeline is empty. Write something publicly to get the ball rolling!' />}
        />
      </Column>
    );
  }

}
